﻿using UnityEngine;
using System.Collections;

public class teleportationParticles : MonoBehaviour {
    public Vector3 startPos;
    public Vector3 endPos;
    public float teleportationTime;

    private float startTime;

	// Use this for initialization
	void Start () {
        startTime = Time.realtimeSinceStartup;
        transform.position = startPos;
	}
	
    public void init(Vector3 spos, Vector3 epos, float tt)
    {
        startPos = spos;
        endPos = epos;
        teleportationTime =tt;
    }
	// Update is called once per frame
	void Update () {
        float delta = Time.realtimeSinceStartup - startTime;
        if (delta < teleportationTime)
            transform.position = Vector3.Lerp(startPos, endPos, delta / teleportationTime);
        else
            Destroy(this.gameObject);
	}
}
