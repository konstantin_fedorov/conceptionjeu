﻿using UnityEngine;
using System.Collections;

public class Warp : MonoBehaviour
{
    public KeyCode OnKey;
    public bool KeepMomentum = false;
    public float MaxDistance = 10.0f;
    public float Cooldown;
    public GameObject Ghost;
    public Color GhostDisabledColor = new Color(1, 0.3f, 0.3f, 0.7f);
    public Color GhostEnabledColor = new Color(0.3f, 0.3f, 1, 0.7f);
    public bool VanishWhenFar = true;
    public float teleportWaitTime = 1.0f;
    public GameObject teleportationParticles;
    public GameObject cooldownEndParticles;

    private int _isColliding;
    private bool _isTeleporting = false;
    private bool _teleportable = true;
    private float _lastActivation;


    void Start()
    {
        var rend = Ghost.GetComponent<SpriteRenderer>();
        rend.color = GhostEnabledColor;
        HideGhost();
        _lastActivation = -Cooldown;
        var collisionHandler = Ghost.AddComponent<triggerParentOnCollision>();
        collisionHandler.collisionEnter += GhostColliderEnter;
        collisionHandler.collisionExit += GhostColliderLeave;
    }

    void GhostColliderEnter(Collider2D collider)
    {
        if (collider.gameObject != gameObject)
            _isColliding++;
    }

    void GhostColliderLeave(Collider2D collider)
    {
        if (collider.gameObject != gameObject)
            _isColliding--;
    }

    void Update()
    {
        if (Input.GetKey(OnKey))
        {
            UpdateColor();
            UpdatePosition();
        }
        else
        {
            HideGhost();
        }
    }

    private void UpdatePosition()
    {
        if (_isTeleporting) return;

        var pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pz.z = transform.position.z;
        var displacementVector = (pz - transform.position);
        var distance = displacementVector.magnitude;
        if(distance > MaxDistance)
        {
            if (VanishWhenFar)
            {
                HideGhost();
                return;
            }
            else
            {
                pz = transform.position + displacementVector.normalized * MaxDistance;
            }
        }

        Ghost.SetActive(true);
        Ghost.transform.position = pz;
        if (Input.GetMouseButtonDown(0) && _teleportable && _isColliding == 0)
        {
            HideGhost();
            var rb = gameObject.GetComponent<Rigidbody2D>();
            if (!KeepMomentum)
            {
                rb.velocity = new Vector3(0, 0, 0);
                rb.angularVelocity = 0;
            }
            StartCoroutine(teleport(rb.velocity, rb.angularVelocity, pz));
        }
    }

    private void UpdateColor()
    {
        var rend = Ghost.GetComponent<SpriteRenderer>();
        if (Time.realtimeSinceStartup < Cooldown + _lastActivation)
        {
            rend.color = qerp(GhostDisabledColor, GhostEnabledColor,
                (Time.realtimeSinceStartup - _lastActivation)/Cooldown);
            _teleportable = false;
        }
        else
        {
            if (!_teleportable && Ghost.activeInHierarchy)
            {
                var particles = Instantiate(cooldownEndParticles);
                particles.GetComponent<FollowTarget>().target = Ghost.transform;
                Destroy(particles, 1);
            }
            _teleportable = true;
            rend.color = GhostEnabledColor;
        }
        if (_isColliding != 0)
        {
            rend.color = GhostDisabledColor;
        }
    }


    private void HideGhost()
    {
        Ghost.SetActive(false);
        _isColliding = 0;
    }

    public static Color qerp(Color from, Color to, float t)
    {
        return from + (to - from) * Mathf.Pow(t, 6);
    }

    IEnumerator teleport(Vector3 speed, float angularSpeed, Vector3 position)
    {
        _isTeleporting = true;
        //Create particles
        var particles = Instantiate(teleportationParticles);
        var particlesScript = particles.GetComponent<teleportationParticles>();
        particlesScript.init(transform.position, position, teleportWaitTime);
        Camera.main.GetComponent<CameraFollow>().follow = particles.transform;

        var sprites = GetComponent<SpriteRenderer>();
        var playerCharacter = GetComponent<PlatformerCharacter2D>();
        var controls = GetComponent<Platformer2DUserControl>();

        var showMaxHeight = playerCharacter.showMaxJumpHeight;
        playerCharacter.showMaxJumpHeight = false;
        GetComponent<Rigidbody2D>().Sleep();
        sprites.enabled = false;
        playerCharacter.enabled = false;
        controls.enabled = false;

        var rb = gameObject.GetComponent<Rigidbody2D>();
        yield return new WaitForSeconds(teleportWaitTime);
        _lastActivation = Time.realtimeSinceStartup;
        transform.position = position;
        rb.velocity = speed;
        rb.angularVelocity = angularSpeed;
        GetComponent<PlatformerCharacter2D>().enabled = true;

        GetComponent<Rigidbody2D>().WakeUp();
        sprites.enabled = true;
        playerCharacter.enabled = true;
        controls.enabled = true;
        playerCharacter.showMaxJumpHeight = showMaxHeight;

        //Follow player now
        Camera.main.GetComponent<CameraFollow>().follow = gameObject.transform;
        _isTeleporting = false;
    }
}
