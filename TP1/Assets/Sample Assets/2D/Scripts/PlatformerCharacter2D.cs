﻿using UnityEngine;
using System.Collections;

public class PlatformerCharacter2D : MonoBehaviour 
{
	bool facingRight = true;							// For determining which way the player is currently facing.

	[SerializeField] float maxSpeed = 10f;				// The fastest the player can travel in the x axis.
    [SerializeField] float acceleration = 90f;          // How fast the player can reach is max speed, aka the acceleration.

	[Range(0, 1)]
	[SerializeField] float crouchSpeed = .36f;			// Amount of maxSpeed applied to crouching movement. 1 = 100%
	
	[SerializeField] float airControl = 0f;			    // Control of a player while jumping;
    [SerializeField] int maxJump = 2;                   // Maximum number consecutive of jumps possible before hitting the ground, including airjumps and the original ground jump. 0 means no limit
	[SerializeField] LayerMask whatIsGround;            // A mask determining what is ground to the character

    [SerializeField]
    float jumpTime = 1.0f;
    [SerializeField]
    float jumpForce;

    [SerializeField]
    public bool showMaxJumpHeight;

    Transform groundCheck;								// A position marking where to check if the player is grounded.
	float groundedRadius = .1f;							// Radius of the overlap circle to determine if grounded
	bool grounded = false;								// Whether or not the player is grounded.
	Transform ceilingCheck;								// A position marking where to check for ceilings
	float ceilingRadius = .01f;							// Radius of the overlap circle to determine if the player can stand up

    Transform leftWallCheck;
    Transform rightWallCheck;
    float walledRadius = 0.1f;


	Animator anim;										// Reference to the player's animator component.
    Rigidbody2D body;
    BoxCollider2D box;
    bool jumping = false;
    bool wasJumping = false;
    bool jumpButtonPressed = false;
    int jumpCount = 0;

    int walled = 0;

    void Awake()
	{
		// Setting up references.
		groundCheck = transform.Find("GroundCheck");
		ceilingCheck = transform.Find("CeilingCheck");
        leftWallCheck = transform.Find("LeftWallCheck");
        rightWallCheck = transform.Find("RightWallCheck");
        anim = GetComponent<Animator>();
        body = GetComponent<Rigidbody2D>();
        box = GetComponent<BoxCollider2D>();
    }


	void FixedUpdate()
	{
		// The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
		grounded = Physics2D.OverlapCircle(groundCheck.position, groundedRadius, whatIsGround);
		anim.SetBool("Ground", grounded);
        if (grounded && !jumping)
        {
            jumpCount = 0;
        }

		// Set the vertical animation
		anim.SetFloat("vSpeed", GetComponent<Rigidbody2D>().velocity.y);

        bool walledRight = Physics2D.Raycast(box.bounds.center, Vector2.right, box.bounds.extents.x + walledRadius, whatIsGround).collider != null;
        bool walledLeft = Physics2D.Raycast(box.bounds.center, Vector2.left, box.bounds.extents.x + walledRadius, whatIsGround).collider != null;
        walled = walledRight ? 1 : (walledLeft ? -1 : 0);
    }


	public void Move(float move, bool crouch, bool jump)
	{
        jumpButtonPressed = jump;
        // If crouching, check to see if the character can stand up
        if (!crouch && anim.GetBool("Crouch"))
		{
			// If the character has a ceiling preventing them from standing up, keep them crouching
			if( Physics2D.OverlapCircle(ceilingCheck.position, ceilingRadius, whatIsGround))
				crouch = true;
		}

        HandleVelocity(move, crouch);

        HandleFlip(move);

        // If the player should jump...
        if (grounded && jumpButtonPressed) {
            // Add a vertical force to the player.
            anim.SetBool("Ground", false);
        }

        if (!wasJumping 
            && jumpButtonPressed 
            && !jumping 
            && (jumpCount < maxJump || maxJump == 0 || walled != 0))
        {
            if (walled == 0 || grounded)
            {
                jumpCount++;
                Debug.Log("Jump count: " + jumpCount);
            }
            StartCoroutine(JumpRoutine());
        }
        wasJumping = jumpButtonPressed;
    }

    void HandleFlip(float direction)
    {
        // If the input is moving the player right and the player is facing left...
        if (direction > 0 && !facingRight)
        {
            // ... flip the player.
            Flip();
        }
        // Otherwise if the input is moving the player left and the player is facing right...
        else if (direction < 0 && facingRight)
        {
            // ... flip the player.
            Flip();
        }
    }

    void Flip()
    {
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void HandleVelocity(float move, bool crouch)
    {
        // Set whether or not the character is crouching in the animator
        anim.SetBool("Crouch", crouch);

        // Reduce the speed if crouching by the crouchSpeed multiplier
        float currentSpeed = body.velocity.x;

        float accel = acceleration * move * (grounded ? 1 : airControl);
        float targetSpeed = crouch ? maxSpeed * crouchSpeed : maxSpeed;

        // The Speed animator parameter is set to the absolute value of the horizontal input.
        anim.SetFloat("Speed", Mathf.Abs(move));

        // Move the character
        body.velocity = new Vector2(Mathf.Clamp(currentSpeed + accel * Time.deltaTime, -targetSpeed, targetSpeed), body.velocity.y);
    }


    IEnumerator JumpRoutine()
    {
        body.velocity = new Vector2((grounded || walled == 0 ? body.velocity.x : -walled * maxSpeed), 0);
        float timer = 0;
        jumping = true;
        while (jumpButtonPressed && timer < jumpTime)
        {
            float proportionCompleted = timer / jumpTime;
            float thisFrameJumpForce = Mathf.Lerp(jumpForce, 0, proportionCompleted);
            body.AddForce(Vector2.up * thisFrameJumpForce);

            timer += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
        jumping = false;
    }

    float GetMaxJumpHeight()
    {
        float timer = 0;
        float velocity = 0;
        float height = 0;
        while (timer < jumpTime && velocity >= 0)
        {
            float proportionCompleted = timer / jumpTime;
            float thisFrameJumpForce = Mathf.Lerp(jumpForce, 0, proportionCompleted);
            velocity += (thisFrameJumpForce + GetComponent<Rigidbody2D>().gravityScale * Physics2D.gravity.y) * Time.fixedDeltaTime;
            height += Mathf.Max(velocity * Time.fixedDeltaTime, 0);
            timer += Time.fixedDeltaTime;
        }

        while (velocity > 0)
        {
            velocity += GetComponent<Rigidbody2D>().gravityScale * Physics2D.gravity.y * Time.fixedDeltaTime;
            height += Mathf.Max(velocity * Time.fixedDeltaTime, 0);
        }

        return height;
    }

    void OnDrawGizmos()
    {
        if(showMaxJumpHeight)
        {
            Gizmos.color = Color.red;
            float maxJumpHeight = GetMaxJumpHeight();
            CircleCollider2D feetCollider = GetComponent<CircleCollider2D>();
            Vector3 feetPosition = transform.position + Vector3.up * (feetCollider.offset.y - feetCollider.radius) * transform.lossyScale.y;
            Gizmos.DrawLine(feetPosition + Vector3.up * maxJumpHeight + Vector3.left, feetPosition + Vector3.up * maxJumpHeight + Vector3.right);
        }
    }
}
