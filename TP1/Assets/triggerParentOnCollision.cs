﻿using UnityEngine;
using System;

public class triggerParentOnCollision : MonoBehaviour {

    public event Action<Collider2D> collisionEnter; 
    public event Action<Collider2D> collisionExit; 

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collisionEnter != null)
        {
            collisionEnter(collision);
        }
    }
    void OnTriggerExit2D(Collider2D collision)
    {
        if (collisionExit != null)
        {
            collisionExit(collision);
        }
    }
}
