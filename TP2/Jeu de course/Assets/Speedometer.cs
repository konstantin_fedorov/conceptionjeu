﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class Speedometer : MonoBehaviour
{

    [SerializeField] private CarController m_car;
    [SerializeField]
    private float minAngle;
    [SerializeField]
    private float maxAngle;
    [SerializeField]
    private float minSpeed;
    [SerializeField]
    private float maxSpeed;

    private RectTransform needle;

    // Use this for initialization
    void Start ()
    {

        needle = GetComponent<RectTransform>();
        needle.eulerAngles = new Vector3(180, 0, minAngle);

    }
	
	// Update is called once per frame
	void Update ()
	{
	    var curAngle = (Mathf.Min(m_car.CurrentSpeed, maxSpeed) - minSpeed)/(maxSpeed - minSpeed)*(maxAngle - minAngle) + minAngle;
        needle.eulerAngles = new Vector3(180, 0, curAngle);
    }
}
