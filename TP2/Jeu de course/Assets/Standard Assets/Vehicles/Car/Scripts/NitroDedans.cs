﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class NitroDedans : MonoBehaviour 
{
    public CarController player;
    
	void Update ()
    {
        transform.localScale = new Vector3(1, player.TurboCount / player.TurboMax, 1);
	}
}
