﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class DangerZone : MonoBehaviour
{

    void OnTriggerStay(Collider other)
    {
        CarController car = other.GetComponentInParent<CarController>();
        if (car != null)
        {
            Score score = car.GetComponent<Score>();
            if (score != null)
            {
                score.addDangerZoneScore();
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        CarController car = other.GetComponentInParent<CarController>();
        if (car != null)
        {
            Score score = car.GetComponent<Score>();
            if (score != null)
            {
                score.quitDangerZoneScore();
            }
        }
    }
}
