﻿using UnityEngine;
using System;
using UnityStandardAssets.Vehicles.Car;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(CarController))]
public class Score : MonoBehaviour {
    [SerializeField] public float m_airtimeScore = 0.5f; // How many points to give per frame of airtime
    [SerializeField] public float m_dangerScore = 0.1f; // How many points to give per frame dangerZone
    [SerializeField]
    public int m_minDangerScore = 60; // Minimum points to add
    [SerializeField]
    public int m_minAirTimeScore = 60; // Minimum points to add
    private CarController m_Car;
    private float airTimeScore = 0f;
    private float dangerScore = 0f;
    private Text scoreString;
    public Text TotalScore;
    private bool wasDisabled = false;
    private int totalScore;

    void Start () 
    {
        m_Car = GetComponent<CarController>();
        scoreString = GameObject.Find("ScoreText").GetComponent<Text>();
        totalScore = 0;
        scoreString.text="";
    }

    void Update () 
    {
        if (!m_Car.BodyGrounded && !m_Car.WheelsGrounded)
        {
            if(!wasDisabled) // After reset, prevent airtime
                addAirTimeScore();
        } else
        {
            resetAirtimeScore();
        }

    }


    // Adds score according to how long the user is in the air. Only starts adding points after 30 frames.
    void addAirTimeScore()
    {
        if(dangerScore == 0)
            airTimeScore += m_airtimeScore * Time.deltaTime;
    }

    void OnDisable()
    {
        StopCoroutine(HideScore());
        airTimeScore = 0;
        dangerScore = 0;
        scoreString.text = "";
        wasDisabled = true;
        m_Car.WheelsGrounded = false;
    }

    // Adds score to the total and resets current score.
    void resetAirtimeScore()
    {
        if (airTimeScore >= m_minAirTimeScore)
        {
            scoreString.text = "Temps aérien: " + (int)airTimeScore;
            m_Car.AddTurbo(airTimeScore / 100f);
            totalScore += (int)airTimeScore;
            StopCoroutine(HideScore());
            StartCoroutine(HideScore());
        }
        wasDisabled = false;
        airTimeScore = 0;
    }


    public void addDangerZoneScore()
    {
        if(airTimeScore == 0)
            dangerScore += m_dangerScore * Time.deltaTime;
    }

    public void quitDangerZoneScore()
    {
        if (dangerScore >= m_minDangerScore)
        {
            scoreString.text = "Conduite dangereuse: " + (int)dangerScore;
            m_Car.AddTurbo(dangerScore / 100f);
            totalScore += (int) dangerScore;
            StopCoroutine(HideScore());
            StartCoroutine(HideScore());
        }

        dangerScore = 0;
    }
    

    void OnGUI()
    {
        TotalScore.text = "Points de style: " + totalScore;
        if (airTimeScore >= m_minAirTimeScore || dangerScore >= m_minDangerScore)
        {
            var texte = "Temps aérien: " + airTimeScore;
            if (dangerScore >= m_minDangerScore)
                texte = "Conduite dangereuse: " + dangerScore;

            TotalScore.text += "\n" + texte;
        }
    }

    public IEnumerator HideScore()
    {
        yield return new WaitForSeconds(2);
        scoreString.text = "";
    }

}
