﻿using UnityEngine;
using System;

[RequireComponent(typeof(Collider))]
public class CollisionForwarder : MonoBehaviour
{
    public event Action<Collider> OnTriggerEnterEvent;
    public event Action<Collider> OnTriggerExitEvent;
    public event Action<Collider> OnTriggerStayEvent;

    public event Action<Collision> OnCollisionEnterEvent;
    public event Action<Collision> OnCollisionExitEvent;
    public event Action<Collision> OnCollisionStayEvent;

    void OnTriggerEnter(Collider col)
    {
        if (OnTriggerEnterEvent != null)
        {
            OnTriggerEnterEvent(col);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (OnTriggerExitEvent != null)
        {
            OnTriggerExitEvent(col);
        }
    }

    void OnTriggerStay(Collider col)
    {
        if (OnTriggerStayEvent != null)
        {
            OnTriggerStayEvent(col);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (OnCollisionEnterEvent != null)
        {
            OnCollisionEnterEvent(col);
        }
    }

    void OnCollisionExit(Collision col)
    {
        if (OnCollisionExitEvent != null)
        {
            OnCollisionExitEvent(col);
        }
    }

    void OnCollisionStay(Collision col)
    {
        if (OnCollisionStayEvent != null)
        {
            OnCollisionStayEvent(col);
        }
    }

}
