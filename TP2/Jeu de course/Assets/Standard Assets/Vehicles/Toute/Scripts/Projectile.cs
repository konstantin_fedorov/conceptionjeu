﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class Projectile : MonoBehaviour {

    public float Speed;
    protected Rigidbody _body;
    private CarController hittedCar;
    public GameObject Pouf;

    void Awake ()
    {
        _body = GetComponent<Rigidbody>();
        _body.velocity = Speed * transform.forward;
        hittedCar = null;
    }
	

    protected virtual void OnCollisionEnter(Collision coll)
    {
        CarController car = coll.collider.transform.GetComponentInParent<CarController>();

        if (car != null && car != hittedCar)
        {
            hittedCar = car;
            //On stop le car
            car.StopVehicle();
            Destroy(gameObject);
        }
    }

    protected void pouf()
    {
        Instantiate(Pouf, transform.position, transform.rotation);
    }

}
