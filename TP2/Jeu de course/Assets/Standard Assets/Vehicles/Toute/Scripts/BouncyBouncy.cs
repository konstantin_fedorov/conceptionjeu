﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class BouncyBouncy : Projectile
{

    void Start()
    {
        _body.velocity = Vector3.zero;
    }

    void FixedUpdate()
    {
        // Below doesn't work with wall collision
        transform.position = Vector3.MoveTowards(transform.position, transform.position + transform.forward * Speed, Speed * Time.deltaTime);
    }

    protected override void OnCollisionEnter(Collision coll)
    {
        CarController car = coll.collider.transform.GetComponentInParent<CarController>();
        if (car != null)
        {
            car.StopVehicle();
            pouf();
            Destroy(gameObject);
        }
        else if (coll.collider.tag == "Wall")
        {
            // Already handled by Unity?
            Vector3 normal = coll.contacts[0].normal;
            transform.forward = transform.forward - Vector3.Dot(transform.forward, normal) * normal * 2;
        }
        else if (coll.collider.tag == "Destructible")
        {
            // Continue en ligne droite apres avoir break le mur? Rebondit ? Se detruit?
            Destroy(gameObject);
        }
    }

}
