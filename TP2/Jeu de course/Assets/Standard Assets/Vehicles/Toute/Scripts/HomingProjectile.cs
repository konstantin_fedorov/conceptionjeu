﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class HomingProjectile : Projectile
{
    public GameObject Target;
    public CarController User;
    public float RotationSpeed;
    public float MaxAngle;
    public float MaxDistance;


    void Start()
    {
        _body.velocity = Vector3.zero;
    }

    void FixedUpdate()
    {
        if (Target == null)
        {
            GetNearest();
        }
        GotoTarget();

    }

    void GotoTarget()
    {
        // Si le projectile est assez proche du nearest pour pouvoir le suivre, on le suit
        if (Target != null && 
            Vector3.Distance(transform.position, Target.transform.position) < MaxDistance)
        {
            // rotate towards the target
            transform.forward = Vector3.RotateTowards(transform.forward, (Target.transform.position - transform.position).normalized, RotationSpeed * Time.deltaTime, 0.0f);
        }

        transform.position = Vector3.MoveTowards(transform.position, transform.position + transform.forward* Speed, Speed * Time.deltaTime);
    }

    void GetNearest()
    {
        float distanceMin = MaxDistance;
        foreach (CarController car in HomingItem.cars)
        {
            if (car != User)
            {
                Vector3 userToCar = car.transform.position - transform.position;
                if (Vector3.Angle(userToCar, transform.forward) < MaxAngle
                    && userToCar.magnitude < distanceMin)
                {
                    Target = car.gameObject;
                    distanceMin = userToCar.magnitude;
                }
            }
        }
    }

    protected override void OnCollisionEnter(Collision coll)
    {
        CarController car = coll.collider.transform.GetComponentInParent<CarController>();
        if (car != null || coll.collider.tag == "Wall" || coll.collider.tag == "Destructible")
        {
            if (car != null)
            {
                car.StopVehicle();
            }
            pouf();
            Destroy(gameObject);
        }
    }

}
