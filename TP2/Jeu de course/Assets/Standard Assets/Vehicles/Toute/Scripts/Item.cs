﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class Item : MonoBehaviour {

    public GameObject ItemPrefab;

    public CarController user;

    public Vector3 SpawnOffset;
    public Color ParticleColor;
    private ParticleSystem _particles;

    void Awake()
    {
        _particles = GetComponent<ParticleSystem>();
        _particles.startColor = ParticleColor;
    }
    

    public virtual GameObject Use()
    {
        Destroy(this.gameObject);
        var bananaQuestionMark = (GameObject)Instantiate(ItemPrefab, user.transform.TransformPoint(SpawnOffset), user.transform.rotation);
        if (ItemPrefab.name == "Yellow")
        {
            bananaQuestionMark.GetComponent<Trap>().setUser(user);
        }
        return bananaQuestionMark;
    }

}
