﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class BlinkyBlinkyFleche : MonoBehaviour
{
    public CollisionForwarder[] Colliders;
    private Image fleche;
    public float blinkTime = 1;
    private float timer = 0;

    private bool blink = false;
    private bool blinkIn = false;

    void Awake()
    {
        fleche = GetComponent<Image>();
        foreach (CollisionForwarder forwarder in Colliders)
        {
            forwarder.OnTriggerEnterEvent += StartBlink;
            forwarder.OnTriggerExitEvent += EndBlink;
        }
    }
	
	void Update ()
    {
	    if (blink)
        {
            if (blinkIn)
            {
                timer += Time.deltaTime;
                if (timer + blinkTime / 2 > blinkTime)
                {
                    blinkIn = false;
                }
            }
            else
            {
                timer -= Time.deltaTime;
                if (timer < 0)
                {
                    blinkIn = true;
                }
            }
            
            fleche.color = new Color(1, 1, 1, (timer + blinkTime/2) / blinkTime);
        }
	}

    private void StartBlink(Collider mencrisspas)
    {
        if (mencrisspas.GetComponentInParent<CarUserControl>() != null)
        {
            blink = true;
            fleche.enabled = true;
            fleche.color = Color.white;
        }
    }

    private void EndBlink(Collider mencrisspas)
    {
        if (mencrisspas.GetComponentInParent<CarUserControl>() != null)
        {
            blink = false;
            fleche.enabled = false;
        }
    }
}
