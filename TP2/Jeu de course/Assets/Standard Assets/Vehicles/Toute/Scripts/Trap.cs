﻿using System;
using System.Collections;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class Trap : MonoBehaviour
{

    public GameObject Pouf;
    [Range(0.1f, 5)] public float BananeCooldown;
    public int NbOfTurns = 3;
    public float SpeedLossPercentage = 0.5f;
    public CarController user;

    private void Awake()
    {
        GetComponent<Collider>().enabled = false;
    }

    IEnumerator enable()
    {
        yield return new WaitForSeconds(1);
        foreach (var collider in user.GetComponentsInChildren<Collider>())
        {
            Physics.IgnoreCollision(collider, GetComponent<Collider>(), false);
        }
    }

    public void setUser(CarController user)
    {
        this.user = user;
        foreach (var collider in user.GetComponentsInChildren<Collider>())
        {
            Physics.IgnoreCollision(collider, GetComponent<Collider>());
        }
        GetComponent<Collider>().enabled = true;
        StartCoroutine(enable());
    }

    void OnCollisionEnter(Collision coll)
    {

        CarController car = coll.collider.transform.GetComponentInParent<CarController>();
        if (car != null)
        {
            car.EnableBadControls(BananeCooldown, NbOfTurns, SpeedLossPercentage);
            Instantiate(Pouf, transform.position, transform.rotation);
            Destroy(gameObject);
        }
        else if(!(coll.collider is TerrainCollider))
        {
            Physics.IgnoreCollision(coll.collider, GetComponent<Collider>());
        }
    }
}
