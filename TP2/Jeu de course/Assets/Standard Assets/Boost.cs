﻿using UnityEngine;

public class Boost : MonoBehaviour
{
    public float boostTime = 2f;  // boost time in seconds
    public float speedBoost = 0.5f; // between 0 - 1 are good values
}
