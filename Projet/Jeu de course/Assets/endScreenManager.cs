﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class endScreenManager : MonoBehaviour
{
    [SerializeField]
    private GUIText RankText;

    [SerializeField]
    private GUIText Time;

    [SerializeField]
    private GUIText Victory;
    // Use this for initialization
    void Start ()
	{
	    var res = GetComponent<RaceResult>();
	    var finishCars = res.getCarsInOrder();
	    RankText.text = "";

        for (int i = 0; i < finishCars.Count; i++)
	    {
	        RankText.text += finishCars[i] + "\n";
	    }

        var racetime = res.getRaceTime();
        var raceTimeMilis = (int) (racetime*1000);
        var timespan = new TimeSpan(0, 0, 0, 0, raceTimeMilis);

        Time.text = "Temps du vainceur: " + string.Format("{0:00} : {1:00} : {2}", timespan.Minutes, timespan.Seconds, timespan.Milliseconds);

        Victory.text = res.isPlayerVictory() ? "Victoire" : "Defaite";

	}

    void Awake()
    {
        Input.simulateMouseWithTouches = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            SceneManager.LoadScene("boot");
        }
    }
}
