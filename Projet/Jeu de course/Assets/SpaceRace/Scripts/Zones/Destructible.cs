﻿using UnityEngine;
using System.Collections;

public class Destructible : MonoBehaviour
{
    public GameObject Pouf;
    public int HP = 2;
    private int _hpLeft;
    public Material OuchMaterial;

    void Awake ()
    {
        _hpLeft = HP;
	}

    void OnCollisionEnter(Collision coll)
    {
        if (coll.collider.tag == "Item")
        {
            GetComponent<MeshRenderer>().material = OuchMaterial;
            --_hpLeft;
            if (_hpLeft <= 0)
            {
                Instantiate(Pouf, transform.position, transform.rotation);
                Instantiate(Pouf, transform.position, transform.rotation);
                Instantiate(Pouf, transform.position, transform.rotation);
                Destroy(gameObject);
            }
            else
            {
                Instantiate(Pouf, transform.position, transform.rotation);
            }
        }
    }
}
