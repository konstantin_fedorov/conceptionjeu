﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(Collider))]
public class Glace : MonoBehaviour
{
    Dictionary<WheelCollider, float> LesRoues = new Dictionary<WheelCollider, float>();
    public float NewStifness;

    void OnTriggerEnter(Collider coll)
    {
        WheelCollider uneRoue = coll.GetComponent<WheelCollider>();
        if (uneRoue != null)
        {
            LesRoues[uneRoue] = uneRoue.sidewaysFriction.stiffness;
            var temp = uneRoue.sidewaysFriction;
            temp.stiffness = NewStifness;
            uneRoue.sidewaysFriction = temp;
        }
    }

    void OnTriggerExit(Collider coll)
    {
        WheelCollider uneRoue = coll.GetComponent<WheelCollider>();
        if (uneRoue != null)
        {
            var temp = uneRoue.sidewaysFriction;
            temp.stiffness = LesRoues[uneRoue];
            uneRoue.sidewaysFriction = temp;
            LesRoues.Remove(uneRoue);
        }
    }

}
