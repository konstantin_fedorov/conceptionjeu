﻿using UnityEngine;
using System.Collections.Generic;
using UnityStandardAssets.Vehicles.Car;

[RequireComponent(typeof(Collider))]
public class Slowww : MonoBehaviour {

    private Dictionary<CarController, float> _lesParticipants = new Dictionary<CarController, float>();
    private Dictionary<CarController, int> _nbRouesEnCollisionPourController = new Dictionary<CarController, int>();

    public float NewMaxSpeed = 20;

    void OnTriggerEnter(Collider coll)
    {
        WheelCollider uneRoue = coll.GetComponent<WheelCollider>();
        if (uneRoue != null)
        {
            if(_lesParticipants.ContainsKey(uneRoue.transform.GetComponentInParent<CarController>()))
            {
                _nbRouesEnCollisionPourController[uneRoue.transform.GetComponentInParent<CarController>()]++;
            }
            else
            {
                _lesParticipants[uneRoue.transform.GetComponentInParent<CarController>()] = uneRoue.transform.GetComponentInParent<CarController>().MaxSpeed;
                uneRoue.transform.GetComponentInParent<CarController>().MaxSpeed = NewMaxSpeed;
                _nbRouesEnCollisionPourController[uneRoue.transform.GetComponentInParent<CarController>()] = 1;
            }

        }
    }

    void OnTriggerExit(Collider coll)
    {
        WheelCollider uneRoue = coll.GetComponent<WheelCollider>();
        if (uneRoue != null)
        {
            _nbRouesEnCollisionPourController[uneRoue.transform.GetComponentInParent<CarController>()]--;
            if (_nbRouesEnCollisionPourController[uneRoue.transform.GetComponentInParent<CarController>()] <= 0)
            {
                uneRoue.transform.GetComponentInParent<CarController>().MaxSpeed = _lesParticipants[uneRoue.transform.GetComponentInParent<CarController>()];
                _lesParticipants.Remove(uneRoue.transform.GetComponentInParent<CarController>());
                _nbRouesEnCollisionPourController.Remove(uneRoue.transform.GetComponentInParent<CarController>());
            }

        }
    }

}
