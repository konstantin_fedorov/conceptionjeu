﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

[RequireComponent(typeof (CarController))]
public class ResetCar : MonoBehaviour
{
    [SerializeField] public float m_carFlippedResetDelay = 3f;
    [SerializeField] public float m_resetDuration = 5f;
    [SerializeField] public GameObject m_resetParticles;
    [SerializeField] public GameObject m_explodyParticles;

    private CarController m_Car;
    private float flippedTime;
    private bool isReseting = false;
    private MonoBehaviour controls;

    private bool isPlayer = true;
    private AutoCam camera;

    void Start()
    {
        m_Car = GetComponent<CarController>();
        controls = gameObject.GetComponent<CarUserControl>();
        if (controls == null)
        {
            controls = gameObject.GetComponent<CarAIControl>();
            isPlayer = false;
        }
        else
        {
            camera = GameObject.Find("CameraRig").GetComponent<AutoCam>();
        }
    }



    // Update is called once per frame
    void FixedUpdate()
    {
        if (!isReseting)
        {
            if (m_Car.BodyGrounded && m_Car.CurrentSpeed < 5)
            {
                flippedTime += Time.fixedDeltaTime;
            }
            else
            {
                flippedTime =0;
            }

            if (flippedTime > m_carFlippedResetDelay)
            {
                //start coroutine reset
                StartCoroutine(Reset());
            }
        }
    }

    public void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.tag == "Bounds")
        {
            //start coroutine reset
            if (!isReseting)
            {
                StartCoroutine(Reset());
            }
        }
    }


    IEnumerator Reset()
    {
        isReseting = true;
        flippedTime = 0;

        //Explode car maybe?
        DisableCar();
        var particles = Instantiate(m_explodyParticles);
        particles.transform.position = m_Car.transform.position;
        yield return new WaitForSeconds(m_resetDuration/2);
        if (isPlayer)
        {
            camera.followVelocity = false;
            camera.moveSpeed *= 50;
        }
        GetComponent<WaypointProgressTracker>().resetPositionAlongRoute();
        yield return new WaitForSeconds(m_resetDuration/2);
        particles = Instantiate(m_resetParticles);
        particles.transform.position = m_Car.transform.position;
        
        EnableCar();
        yield return new WaitForSeconds(2.5f);


        if (isPlayer)
        {
            camera.followVelocity = true;
            camera.moveSpeed /= 50;
        }
        isReseting = false;
    }

    private void EnableCar()
    {
        transform.FindChild("SkyCar").gameObject.SetActive(true);
        transform.FindChild("Colliders").gameObject.SetActive(true);
        transform.FindChild("Lights").gameObject.SetActive(true);
        transform.FindChild("Particles").gameObject.SetActive(true);
        transform.FindChild("Helpers").gameObject.SetActive(true);
        transform.FindChild("WheelsHubs").gameObject.SetActive(true);
        var scripts = gameObject.GetComponents<MonoBehaviour>();
        foreach (var script in scripts)
        {
            script.enabled = true;
        }
        controls.enabled = true;
        gameObject.GetComponent<Rigidbody>().useGravity = true;
    }

    private void DisableCar()
    {
        var rigidbody = gameObject.GetComponent<Rigidbody>();
        if (m_Car.ItemInventory != null)
        {
            Destroy(m_Car.ItemInventory.gameObject);
            m_Car.ItemInventory = null;
        }
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;
        rigidbody.useGravity = false;
        controls.enabled = false;

        var scripts = gameObject.GetComponents<MonoBehaviour>();
        foreach (var script in scripts)
        {
            if (script != this)
                script.enabled = false;
        }
        transform.FindChild("SkyCar").gameObject.SetActive(false);
        transform.FindChild("Colliders").gameObject.SetActive(false);
        transform.FindChild("Lights").gameObject.SetActive(false);
        transform.FindChild("Particles").gameObject.SetActive(false);
        transform.FindChild("Helpers").gameObject.SetActive(false);
        transform.FindChild("WheelsHubs").gameObject.SetActive(false);
    }
}
