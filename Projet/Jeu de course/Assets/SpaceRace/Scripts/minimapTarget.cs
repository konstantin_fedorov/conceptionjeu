﻿using UnityEngine;
using System.Collections;

public class minimapTarget : MonoBehaviour {

    public GameObject vehicule;


    private Bounds bounds;
    private RectTransform minimap;
    private RectTransform icon;
    private float ratio;

	// Use this for initialization
	void Start () {
        minimap = GameObject.Find("Minimap").GetComponent<RectTransform>();
        bounds = GetComponentInParent<minimap>().trackedZone.GetComponent<BoxCollider>().bounds;
        icon = GetComponent<RectTransform>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        
        var relPos = new Vector2((vehicule.transform.position.x - bounds.min.x) / bounds.size.x * minimap.rect.width,( vehicule.transform.position.z - bounds.min.z)/ bounds.size.z * minimap.rect.height);
        icon.anchoredPosition = new Vector3(relPos.x, relPos.y, 0);
        icon.rotation = Quaternion.identity;
        icon.Rotate(Vector3.forward, -vehicule.gameObject.transform.FindChild("SkyCar").rotation.eulerAngles.y);
	}
}
