﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class HyperdriveText : MonoBehaviour
{

    [SerializeField] private CarController m_car;

    private UnityEngine.UI.Text hdText;

    // Use this for initialization
    void Start ()
    {

        hdText = GetComponent<UnityEngine.UI.Text>();
        hdText.text = "Disengaged";

    }
	
	// Update is called once per frame
	void Update ()
	{
        if (m_car.IsTurboActive)
        {
            hdText.text = "Engaged";
            hdText.color = new Color(0 / 255f, 255 / 255f, 139 / 255f);
        } 
        else if (m_car.TurboCount == 0)
        {
            hdText.text = "Depleted";
            hdText.color = new Color(176 / 255f, 5 / 255f, 5 / 255f);
        } 
        else
        {
            hdText.text = "Disengaged";
            hdText.color = new Color(205 / 255f, 223 / 255f, 245 / 255f);
        }
    }
}
