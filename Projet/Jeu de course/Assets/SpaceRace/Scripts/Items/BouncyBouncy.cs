﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class BouncyBouncy : Projectile
{

    protected override void OnCollisionEnter(Collision coll)
    {
        CarController car = coll.collider.transform.GetComponentInParent<CarController>();
        if (car != null)
        {
            car.StopVehicle();
            pouf();
            Destroy(gameObject);
        }
        else if (coll.collider.tag == "Wall")
        {
            // Already handled by Unity?
            Vector3 normal = coll.contacts[0].normal;
            forward_vect = forward_vect - Vector3.Dot(forward_vect, normal) * normal * 2;
        }
        else if (coll.collider.tag == "Destructible")
        {
            // Continue en ligne droite apres avoir break le mur? Rebondit ? Se detruit?
            Destroy(gameObject);
        }
    }

}
