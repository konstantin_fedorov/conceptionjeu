﻿using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class ItemBoxGenerator : MonoBehaviour
{
    public CollisionForwarder ItemBoxPrefab;
    public float GenerationDelay = 5f;
    private float timer = 0f;
    private bool hasItem = false;

	void Awake ()
    {
        SpawnItemBox();
    }
	
	void Update ()
    {
        if (timer > 0 && !hasItem)
        {
            timer -= Time.deltaTime;

            if (timer <= 0)
            {
                SpawnItemBox();
            }
        }
    }

    void ItemTaken(Collider col)
    {
        if (timer <= 0 && hasItem && col.GetComponentInParent<CarController>()!=null)
        {
            hasItem = false;
            timer = GenerationDelay;
        }
    }

    void SpawnItemBox()
    {
        hasItem = true;
        CollisionForwarder itemBox = Instantiate<CollisionForwarder>(ItemBoxPrefab);
        itemBox.transform.position = transform.position;
        itemBox.OnTriggerEnterEvent += ItemTaken;
        itemBox.transform.parent = gameObject.transform;
    }
}
