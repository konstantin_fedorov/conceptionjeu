﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class Projectile : MonoBehaviour {

    public float Speed = 60;
    public GameObject Pouf;

    protected Vector3 forward_vect;

    void Awake()
    {
        forward_vect = transform.forward;
    }

    protected virtual void FixedUpdate()
    {
        // Below doesn't work with wall collision
        transform.position = Vector3.MoveTowards(transform.position, transform.position + forward_vect * Speed, Speed * Time.deltaTime);
    }


    protected virtual void OnCollisionEnter(Collision coll)
    {
        CarController car = coll.collider.transform.GetComponentInParent<CarController>();

        if (car != null || coll.collider.tag == "Wall" || coll.collider.tag == "Destructible")
        {
            if (car != null)
            {
                //On stop le car
                car.StopVehicle();
            }
            pouf();
            Destroy(gameObject);
        }
    }

    protected void pouf()
    {
        Instantiate(Pouf, transform.position, transform.rotation);
    }

}
