﻿using UnityEngine;
using UnityStandardAssets.Vehicles.Car;


public class HomingItem : Item
{

    public float MaxAngle;
    public float MaxDistance;

    public static CarController[] cars;
    private CarController _nearest;
    public GameObject Target;
    private GameObject _target;

    protected void Start()
    {
        if(cars == null)
        {
            cars = FindObjectsOfType<CarController>();
        }
        if (user.GetComponent<CarUserControl>() != null)
        {
            _target = Instantiate(Target);
            _target.SetActive(false);
        }
    }

    void Update()
    {
        GetNearest();
        if (_target != null)
        {
            if (_nearest != null)
            {
                _target.SetActive(true);
                _target.transform.position = _nearest.transform.position + Vector3.up * 2;
                _target.transform.LookAt(Camera.main.transform.position, -Vector3.up);
                _target.transform.localScale = Vector3.one * (2 + (Camera.main.transform.position - _nearest.transform.position).magnitude / 10);
            }
            else
            {
                _target.SetActive(false);
            }
        }
    }

    public override GameObject Use()
    {
        if (_target != null)
        {
            Destroy(_target.gameObject);
        }

        HomingProjectile homingProjectile = base.Use().GetComponent<HomingProjectile>();
        homingProjectile.User = user;
        homingProjectile.MaxAngle = MaxAngle;
        homingProjectile.MaxDistance = MaxDistance;

        return homingProjectile.gameObject;
    }

    void GetNearest()
    {
        _nearest = null;
        float distanceMin = MaxDistance;
        foreach (CarController car in cars)
        {
            if(car != user)
            {
                Vector3 userToCar = car.transform.position - user.transform.position;
                if (Vector3.Angle(userToCar, user.transform.forward) < MaxAngle
                    && userToCar.magnitude < distanceMin)
                {
                    _nearest = car;
                    distanceMin = userToCar.magnitude;
                }
            }
        }
    }
}
