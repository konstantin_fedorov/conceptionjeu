﻿using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class ItemBox : MonoBehaviour {

    public Item[] PossibleItems;


    void OnTriggerEnter(Collider other)
    {
        CarController car = other.GetComponentInParent<CarController>();
        if (car != null)
        {
            if (car.ItemInventory == null)
            {
                car.ItemInventory = Instantiate<Item>(PossibleItems[Random.Range(0, PossibleItems.Length)]);
                car.ItemInventory.transform.parent = car.transform;
                car.ItemInventory.transform.localPosition = Vector3.zero;
                car.ItemInventory.user = car;
                Destroy(this.gameObject);
            }
        }
    }
}
