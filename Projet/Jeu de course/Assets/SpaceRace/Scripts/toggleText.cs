﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;

public class toggleText : MonoBehaviour
{
    public string key;

    private Text text;
	// Use this for initialization
	void Start ()
	{
        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () 
    {
	    if (CrossPlatformInputManager.GetButtonDown(key))
	    {
	        text.enabled = !text.enabled;
	    }
	}
}
