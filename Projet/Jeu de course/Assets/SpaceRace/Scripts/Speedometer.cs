﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class Speedometer : MonoBehaviour
{

    [SerializeField] private CarController m_car;
    [SerializeField]
    private float minSpeed;
    [SerializeField]
    private float maxSpeed;

    private UnityEngine.UI.Text speedText;

    // Use this for initialization
    void Start ()
    {

        speedText = GetComponent<UnityEngine.UI.Text>();
        speedText.text = "0 %";

    }
	
	// Update is called once per frame
	void Update ()
	{
        float curSpeed;
        if (maxSpeed != 0)
            curSpeed = m_car.CurrentSpeed / maxSpeed;
        else
            curSpeed = 0;
        speedText.text = Mathf.Round(curSpeed * 10000) / 100 + " %";
    }
}
