﻿using UnityEngine;
using System.Collections;

public class CameraRotate : MonoBehaviour {

    public GameObject target;
    public float turnSpeed = 1;

    private Camera m_cam;
	// Use this for initialization
	void Start () {
        m_cam = GetComponent<Camera>();
	}

    // Update is called once per frame
    void Update()
    {
        m_cam.transform.LookAt(target.transform);
        transform.Translate(Vector3.right * turnSpeed * Time.deltaTime);
    }
}
