﻿using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleSystemScaler : MonoBehaviour
{

	void Awake ()
    {
        var shape = GetComponent<ParticleSystem>().shape;
        shape.box = new Vector3(transform.parent.lossyScale.x * shape.box.x, shape.box.y, shape.box.z);
	}
	
}
