﻿using UnityEngine;
using System.Collections;

public class Canon : MonoBehaviour
{
    public Projectile[] ProjectilePrefab;

    //Time between each shot
    public float Cooldown = 3;
    public int NbShot = 1;
    public Transform Pool;
    private float timer = 0;


	void Update ()
    {
        timer += Time.deltaTime;
        if (timer >= Cooldown)
        {
            Shoot();
            timer = 0;
        }
	}


    void Shoot()
    {
        for (int i = 0; i < NbShot; i++)
        {
            transform.Rotate(Vector3.up * (360.0f / NbShot));
            Projectile clone = (Projectile)Instantiate(ProjectilePrefab[Random.Range(0,ProjectilePrefab.Length)], transform.position, transform.rotation);
            clone.transform.parent = Pool;
        }
    }


}
