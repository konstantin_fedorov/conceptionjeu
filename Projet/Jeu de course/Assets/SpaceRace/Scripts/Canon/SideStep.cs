﻿using UnityEngine;
using System.Collections;

public class SideStep : MonoBehaviour
{
    public float Speed;
    public float Distance;

    private float _distance;
    private bool _onTheWayBack;
	

	void Update ()
    {
        float speed = (Speed + (Speed/2 * Mathf.Sin(Time.time))) * Time.deltaTime;
        transform.position = transform.position + ((_onTheWayBack ? -transform.right : transform.right) * speed);
        _distance += speed;
        if (_distance >= Distance)
        {
            _distance = 0;
            _onTheWayBack = !_onTheWayBack;
        }
    }
}
