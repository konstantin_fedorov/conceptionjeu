﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Vehicles.Car;
using System.Linq;

public class RaceManager : MonoBehaviour 
{


	[SerializeField]
	private GameObject _carContainer;

	[SerializeField]
	private GUIText _announcement;

	[SerializeField]
	private int _timeToStart;

	[SerializeField]
	private int _endCountdown;

    private float startTime;

	// Use this for initialization
	void Awake () 
	{
		CarActivation(false);

	}
	
	void Start()
	{
		StartCoroutine(StartCountdown());
    }

	IEnumerator StartCountdown()
	{
		int count = _timeToStart;
		do 
		{
			_announcement.text = count.ToString();
			yield return new WaitForSeconds(1.0f);
			count--;
		}
		while (count > 0);
		_announcement.text = "Partez!";
	    startTime = Time.time;
		CarActivation(true);
		yield return new WaitForSeconds(1.0f);
		_announcement.text = "";
	}

	public void EndRace(List<CarController> winner)
	{
		StartCoroutine(EndRaceImpl(winner));
	}

	IEnumerator EndRaceImpl(List<CarController> carsInOrder)
	{
		CarActivation(false);
		_announcement.fontSize = 20;
	    var endTime = Time.time;

        _announcement.text = "Defaite: ";
        if (carsInOrder[0].GetComponent<CarUserControl>() != null)
            _announcement.text = "Victoire: ";

        _announcement.text += carsInOrder[0].name + " en premiere place. ";
		yield return new WaitForSeconds(_endCountdown);

	    RaceResult res = GetComponent<RaceResult>();
	    res.setRaceResults(endTime-startTime, carsInOrder.Select(x => x.gameObject.name).ToList(), carsInOrder[0].GetComponent<CarUserControl>() != null);
        Application.LoadLevel("End");
	}

	public void Announce(string announcement, float duration = 2.0f)
	{
		StartCoroutine(AnnounceImpl(announcement,duration));
	}

	IEnumerator AnnounceImpl(string announcement, float duration)
	{
		_announcement.text = announcement;
		yield return new WaitForSeconds(duration);
		_announcement.text = "";
	}

	public void CarActivation(bool activate)
	{
		foreach (CarAIControl car in _carContainer.GetComponentsInChildren<CarAIControl>(true))
		{
			car.enabled = activate;
		}
		
		foreach (CarUserControl car in _carContainer.GetComponentsInChildren<CarUserControl>(true))
		{
			car.enabled = activate;
		}

	}
}
