﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SimpleBlink : MonoBehaviour {

    private Renderer fleche;
    public float blinkTime = 2;
    private float timer = 0;

    private bool blink = true;
    private bool blinkIn = false;

    void Awake()
    {
        fleche = GetComponent<Renderer>();
    }

    void Update()
    {
        if (blink)
        {
            if (blinkIn)
            {
                timer += Time.deltaTime;
                if (timer + blinkTime / 2 > blinkTime)
                {
                    blinkIn = false;
                }
            }
            else
            {
                timer -= Time.deltaTime;
                if (timer < 0)
                {
                    blinkIn = true;
                }
            }

            fleche.material.SetColor("_Color", new Color(1, 1, 1, (timer + blinkTime / 2) / blinkTime));
        }
    }
}
