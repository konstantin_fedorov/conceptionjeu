﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StartScreenManager : MonoBehaviour 
{
	void Awake()
	{
		Input.simulateMouseWithTouches = true;
	}

	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
		{
			SceneManager.LoadScene("SPRACE");
		}
	}
}
