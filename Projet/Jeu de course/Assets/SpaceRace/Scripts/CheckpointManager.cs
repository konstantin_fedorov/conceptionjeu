﻿using UnityEngine;
using System.Collections.Generic;
using UnityStandardAssets.Vehicles.Car;
using System.Linq;
using UnityEngine.UI;

public class CheckpointManager : MonoBehaviour
{

    [SerializeField]
    private GameObject _carContainer;

    [SerializeField]
    private int _checkPointCount;
    [SerializeField]
    private int _totalLaps;

    [SerializeField]
    WaypointCircuit circuit;         // A reference to the waypoint-based route indicating the position of the cars

    private bool _finished = false;

    [SerializeField]
    private Text textField;

    private Dictionary<CarController, PositionData> _carPositions = new Dictionary<CarController, PositionData>();

    private class PositionData
    {
        public int lap;
        public int checkPoint;
        public float progressDistance;			// The progress round the route, used in smooth mode.
        public Vector3 lastPosition;           // Used to calculate current speed (since we may not have a rigidbody component)
        public float speed;					// current speed of this object (calculated from delta since last frame)
    }

    // Use this for initialization
    void Awake()
    {
        foreach (CarController car in _carContainer.GetComponentsInChildren<CarController>(true))
        {
            var newPosData = new PositionData();
            newPosData.checkPoint = _checkPointCount - 1;
            newPosData.lap = -1;
            _carPositions[car] = newPosData;
        }
    }

    public void CheckpointTriggered(CarController car, int checkPointIndex)
    {

        PositionData carData = _carPositions[car];

        if (!_finished)
        {
            if (checkPointIndex == 0)
            {
                if (carData.checkPoint == _checkPointCount - 1)
                {
                    carData.checkPoint = checkPointIndex;
                    carData.lap += 1;
                    Debug.Log(car.name + " lap " + carData.lap);
                    if (carData.lap >= _totalLaps)
                    {
                        _finished = true;
                        GetComponent<RaceManager>().EndRace(getCarsInOrder());
                    } else if (IsPlayer(car))
                    {
                        GetComponent<RaceManager>().Announce("Tour " + (carData.lap + 1).ToString());
                    }

                    
                }
            }
            else if (carData.checkPoint == checkPointIndex - 1) //Checkpoints must be hit in order
            {
                carData.checkPoint = checkPointIndex;
            }
        }
    }

    bool IsPlayer(CarController car)
    {
        return car.GetComponent<CarUserControl>() != null;
    }

    // Update the position of all the cars
    void Update()
    {
        var cars = new List<CarController>(_carPositions.Keys);
        float maxProgress = float.MinValue;
        foreach ( var car in cars)
        {
            var posInfo = _carPositions[car];
            if (posInfo.lap >= 0)
            {
                if (Time.deltaTime > 0)
                {
                    posInfo.speed = Mathf.Lerp(posInfo.speed, (posInfo.lastPosition - car.transform.position).magnitude / Time.deltaTime, Time.deltaTime);
                }
                // get our current progress along the route
                var progressPoint = circuit.GetRoutePoint(posInfo.progressDistance);
                Vector3 progressDelta = progressPoint.position - car.transform.position;
                if (Vector3.Dot(progressDelta, progressPoint.direction) < 0)
                {
                    posInfo.progressDistance += progressDelta.magnitude * 0.5f;
                }

                posInfo.lastPosition = car.transform.position;
                maxProgress = Mathf.Max(posInfo.progressDistance, maxProgress);
                _carPositions[car] = posInfo;
            }
        }

        //update position text
        string order = "\n";
        int i = 0;
        foreach (var otherCar in getCarsInOrder())
        {
            if (_carPositions[otherCar].lap >= 0)
            {
                i++;
                order += i + "    " + otherCar.name + "\n";
                otherCar.SetRubberbandFactor((int)(maxProgress - _carPositions[otherCar].progressDistance));
            }
        }
        textField.text = order;
    }

    private List<CarController> getCarsInOrder()
    {
        return _carPositions.OrderByDescending(x => x.Value.progressDistance).Select(x => x.Key).ToList();
    }
}
