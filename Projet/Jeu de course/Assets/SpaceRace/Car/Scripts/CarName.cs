﻿using UnityEngine;

public class CarName : MonoBehaviour
{
	
    void Awake()
    {
        GetComponent<TextMesh>().text = transform.parent.name;
    }

	void Update ()
    {
        transform.position = transform.parent.position + Vector3.up * 2;
        transform.LookAt(Camera.main.transform.position, Vector3.up);
    }
}
