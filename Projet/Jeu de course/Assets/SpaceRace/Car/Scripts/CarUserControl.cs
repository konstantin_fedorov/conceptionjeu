using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof (CarController))]
    public class CarUserControl : MonoBehaviour
    {
        private CarController m_Car; // the car controller we want to use


        private void Awake()
        {
            // get the car controller
            m_Car = GetComponent<CarController>();
        }

        private void Update()
        {

            if (CrossPlatformInputManager.GetButtonDown("Fire2"))
                m_Car.SetTurbo(true);
            else if (CrossPlatformInputManager.GetButtonUp("Fire2"))
                m_Car.SetTurbo(false);
        }

        private void FixedUpdate()
        {
            // pass the input to the car!
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");
#if !MOBILE_INPUT
            float handbrake = CrossPlatformInputManager.GetAxis("Handbrake");
            bool jump = CrossPlatformInputManager.GetButton("Jump");
            m_Car.Move(h, v, v, handbrake, jump);
            if (CrossPlatformInputManager.GetButton("Fire1"))
            {
                m_Car.TryUseItem();
            }
#else
            m_Car.Move(h, v, v, 0f);
#endif
            if (!m_Car.BodyGrounded && !m_Car.WheelsGrounded)
            {
                float h2 = -CrossPlatformInputManager.GetAxis("Horizontal2");
                float v2 = CrossPlatformInputManager.GetAxis("Vertical2");
                float yaw = CrossPlatformInputManager.GetAxis("Yaw");
                var transform = gameObject.transform;
                transform.Rotate(new Vector3(v2 * 5, yaw * 5, h2 * 5));
            }

        }
    }
}
