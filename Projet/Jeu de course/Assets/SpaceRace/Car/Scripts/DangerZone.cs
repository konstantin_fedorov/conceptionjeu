﻿using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class DangerZone : MonoBehaviour
{
    public bool TagZone = false;
    void OnTriggerEnter(Collider other)
    {
        if (TagZone)
        {
            CarController car = other.GetComponentInParent<CarController>();
            if (car != null)
            {
                Score score = car.GetComponent<Score>();
                if (score != null)
                {
                    score.AddTagDangerZoneScore();
                }
            }
        }

    }

    void OnTriggerStay(Collider other)
    {
        if (!TagZone)
        {
            CarController car = other.GetComponentInParent<CarController>();
            if (car != null)
            {
                Score score = car.GetComponent<Score>();
                if (score != null)
                {
                    score.addDangerZoneScore();
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        CarController car = other.GetComponentInParent<CarController>();
        if (car != null)
        {
            Score score = car.GetComponent<Score>();
            if (score != null && !TagZone)
            {
                score.quitDangerZoneScore();
            }
        }
    }
}
