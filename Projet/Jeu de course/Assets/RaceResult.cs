﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class RaceResult : MonoBehaviour
{
    public static List<String> names;
    public static float raceTime;
    public static bool playerVictory;

    public void setRaceResults(float time, List<String> carNames, bool playerVict)
    {
        names = carNames;
        raceTime = time;
        playerVictory = playerVict;
    }

    public List<String> getCarsInOrder()
    {
        return names;
    }

    public float getRaceTime()
    {
        return raceTime;
    }

    internal bool isPlayerVictory()
    {
        return playerVictory;
    }
}
